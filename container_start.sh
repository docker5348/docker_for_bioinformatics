xhost local:root
docker run \
	-it \
	--privileged \
	--net host \
	--ipc host \
	-e DISPLAY=$DISPLAY \
	-v ~/ros2_bioinformatics_ws:/ros_ws \
	-v ~/ros2_bioinformatics_ws/src/:/ros_ws/src \
	ros2_bio:Dockerfile
