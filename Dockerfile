# This is an auto generated Dockerfile for ros:desktop-full
# generated from docker_images_ros2/create_ros_image.Dockerfile.em
FROM osrf/ros:humble-desktop-jammy
ENV DISPLAY $DISPLAY
# install ros2 packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    ros-humble-desktop-full=0.10.0-1* \
    && rm -rf /var/lib/apt/lists/*
RUN apt update -y && apt upgrade -y
RUN apt install software-properties-common -y
RUN add-apt-repository universe
RUN apt install pip -y
RUN pip install ros2-numpy
RUN apt install ros-humble-ros2bag ros-humble-rosbag2-storage-default-plugins libfuse2 -y
RUN apt install python3-colcon-common-extensions -y
RUN apt install wget -y
# get Rust
RUN curl https://sh.rustup.rs -sSf | bash -s -- -y
RUN pip3 install opengen
ENV PATH="/root/.cargo/bin:${PATH}"
RUN echo 'source $HOME/.cargo/env' >> $HOME/.bashrc
RUN cargo install cargo-outdated
RUN cargo install evcxr_repl
RUN cargo install mdbook
RUN curl --proto '=https' --tlsv1.2 -fsSL https://drop-sh.fullyjustified.net | bash -s -- -y

# get JULIA
RUN curl -fsSL https://install.julialang.org | bash -s -- -y
RUN pip3 install --user --upgrade git+https://github.com/colcon/colcon-cargo.git
RUN echo "eval '$(register-python-argcomplete3 ros2)'" >> $HOME/.bashrc
RUN echo "eval '$(register-python-argcomplete3 colcon)'" >> $HOME/.bashrc
VOLUME /ros_ws
VOLUME /ros_ws/src

